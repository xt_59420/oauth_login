package oauth_login

import (
	"encoding/json"
	"fmt"
	"gitee.com/xt_59420/oauth_login/utils"
	"io/ioutil"
	"net/http"
)

// 安卓、ios app授权登陆

// FacebookResp facebook token认证的返回参数
type FacebookResp struct {
	Data *FacebookData `json:"data"`
}

type FacebookData struct {
	AppId     string `json:"app_id"`
	IsValid   bool   `json:"is_valid"`   // 是否验证
	ExpiresAt int32  `json:"expires_at"` // 过期时间
	UserId    string `json:"user_id"`    // 玩家编号
}

// FacebookUserResp facebook 玩家的信息
type FacebookUserResp struct {
	Id        string               `json:"id"`        // 应用用户的应用范围用户 ID
	Email     string               `json:"email"`     // 电子邮件地址|没有可用的有效电子邮件地址，则不会返回此字段。
	Languages string               `json:"languages"` // 此人知道的语言
	Picture   *FacebookPictureResp `json:"picture"`   // 头像信息
	Name      string               `json:"name"`      // 全名
}

type FacebookPictureResp struct {
	Url string `json:"url"` // 头像地址
}

// RequestFacebookToken 验证facebook token、获取用户信息
func RequestFacebookToken(inputToken, facebookAppId, facebookAppSecret string) (resp *FacebookUserResp, err error) {
	curTime := utils.UnixTime()

	// 应用编号为空
	if facebookAppId == "" {
		return nil, fmt.Errorf("app ID is empty, error: %w", err)
	}

	// 应用密钥为空
	if facebookAppSecret == "" {
		return nil, fmt.Errorf("app secret is empty, error: %w", err)
	}

	// 访问令牌
	if inputToken == "" {
		return nil, fmt.Errorf("inputToken is empty, error: %w", err)
	}

	// 验证玩家token|facebook 应用编号、facebook应用密钥
	accessToken := fmt.Sprintf("%s|%s", facebookAppId, facebookAppSecret)
	response, err := http.Get("https://graph.facebook.com/debug_token?input_token=" + inputToken + "&access_token=" + accessToken)
	if err != nil {
		return nil, fmt.Errorf("the incoming value is empty, error: %w", err)
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	facebookRep := &FacebookResp{}
	_ = json.Unmarshal(body, &resp)
	// 验证响应的结果
	if facebookRep.Data == nil || !facebookRep.Data.IsValid || facebookRep.Data.ExpiresAt < curTime {
		return nil, fmt.Errorf("debug_token body nil, error: %w", err)
	}

	// 请求玩家公开的信息
	fields := "id,email,languages,picture,name"
	url := fmt.Sprintf("https://graph.facebook.com/me?fields=%s&access_token=%s", fields, inputToken)
	meResponse, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer meResponse.Body.Close()

	meBody, _ := ioutil.ReadAll(meResponse.Body)
	resp = &FacebookUserResp{}
	_ = json.Unmarshal(meBody, &resp)

	return resp, nil
}
