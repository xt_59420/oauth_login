package utils

import "time"

// UnixTime 时间戳（秒）
func UnixTime() int32 {
	return int32(time.Now().Unix())
}
