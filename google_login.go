package oauth_login

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

// 安卓、ios app授权登陆

// GoogleBodyResp 接收谷歌响应
type GoogleBodyResp struct {
	Sub           string `json:"sub"`            // 玩家id
	Aud           string `json:"aud"`            // 谷歌申请的客户端id
	Email         string `json:"email"`          // 邮箱
	EmailVerified string `json:"email_verified"` // 是否验证
	Name          string `json:"name"`           // 昵称
	Picture       string `json:"picture"`        // 头像
	Locale        string `json:"locale"`         // 语言
}

// RequestGoogleToken 验证谷歌token
func RequestGoogleToken(idToken string) (resp *GoogleBodyResp, err error) {
	if idToken == "" {
		return nil, fmt.Errorf("the incoming value is empty, error: %w", err)
	}
	// 请求获取
	response, err := http.Get("https://oauth2.googleapis.com/tokeninfo?id_token=" + idToken)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)

	resp = &GoogleBodyResp{}
	err = json.Unmarshal(body, resp)

	return resp, nil
}
